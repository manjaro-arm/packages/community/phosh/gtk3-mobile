# Maintainer: Philip Müller <philm@manjaro.org>
# Contributor: Danct12 <danct12@disroot.org>
# Contributor Jan Alexander Steffens (heftig) <heftig@archlinux.org>
# Contributor: Ionut Biru <ibiru@archlinux.org>

pkgname=gtk3-mobile
pkgver=3.24.48
pkgrel=1
pkgdesc="GObject-based multi-platform GUI toolkit (Built with Purism patches)"
arch=('x86_64' 'armv7h' 'aarch64')
url="https://www.gtk.org/"
license=(LGPL-2.1-or-later)
depends=(
  adwaita-icon-theme
  at-spi2-core
  cairo
  cantarell-fonts
  dconf
  desktop-file-utils
  fontconfig
  fribidi
  gdk-pixbuf2
  glib2
  glibc
  harfbuzz
  iso-codes
  libcloudproviders
  libcolord
  libcups
  libegl
  libepoxy
  libgl
  librsvg
  libx11
  libxcomposite
  libxcursor
  libxdamage
  libxext
  libxfixes
  libxi
  libxinerama
  libxkbcommon
  libxrandr
  libxrender
  pango
  shared-mime-info
  tinysparql
  wayland
)
makedepends=(
  git
  glib2-devel
  gobject-introspection
  gtk-doc
  hicolor-icon-theme
  meson
  sassc
  wayland-protocols
)
source=(
  "git+https://gitlab.gnome.org/GNOME/gtk.git#tag=$pkgver"
  gtk-query-immodules-3.0.hook
  0001-Allow-disabling-legacy-Tracker-search.patch

  # Purism patches
  Add-GtkHdyViewSwitcherButton.patch
  Add-GtkHdyViewSwitcher.patch
  Add-GtkHdyViewSwitcherBar.patch
  Add-GtkHdyAnimation.patch
  Add-GtkHdySqueezer.patch
  Add-GtkHdyViewSwitcherTitle.patch
  Add-GtkHdyShadowHelper.patch
  Add-GtkHdyNavigationDirection.patch
  Add-GtkHdySwipeable-and-GtkHdySwipeTracker.patch
  Add-GtkHdyClamp.patch
  Add-GtkHdyFlap.patch
  theme-Add-libhandy-styles.patch
  Add-padding-for-HdyViewSwitcherTitle.patch
  hdy-flap-Use-natural-size-for-folding-instead-of-minimum.patch
  Add-org.gtk.Settings.Purism.patch
  gtkprivate-Add-an-API-to-check-if-phone.patch
  aboutdialog-Port-to-phones.patch
  Add-the-view-sidebar-symbolic-icon.patch
  Port-file-chooser-to-phones.patch
  messagedialog-Set-orientation-based-on-device.patch
  dialog-Maximize-resizable-dialogs-on-phones.patch
  window-Maximize-resizable-pseudo-dialogs-on-mobile.patch
  headerbar-Use-a-back-button-in-dialogs-on-mobile.patch
  infobar-Move-the-action-area-below-on-the-phone.patch
  Reduce-the-font-chooser-minimum-size.patch
  printunixdialog-Adapt-for-phones.patch
  window-Disable-window-dragging-on-phones.patch
  librem5-Make-GtkShortcutsWindow-adaptive.patch
  scrolledwindow-Set-deceleration-value-based-on-the-device.patch
  events-Compress-touch-update-events.patch
  gdk-wayland-Track-last-touch-serial-on-seat.patch
)
sha256sums=('afe99fb376095586c34aa8a1127003b043a315f1128a4ae0b15d2afd7485556e'
            'a0319b6795410f06d38de1e8695a9bf9636ff2169f40701671580e60a108e229'
            '3a875310f454d8cd15cc126517a3c986bf86d3f50521d7435d6852abe4b7b134'
            '1e3ec101a72a895466509b0ce12e4c8a087f2017e28e7d0159b5bfdb8a651096'
            'b44a9a640dccc06efda6a47be0d0ccbc3109d99c575f1868640f5c6ee2b4a0f0'
            '7d854b1c8b404425fa4caf560bc3ac288eb8efb5d49151b8f60d2115e5f27978'
            '627616e1d65cc036a0ae839f27ec6716399c2e4afe458f2c11c3cf088759af1d'
            'beae40187954ebaed69b739aedae2199dc8a5589be48ab2ced99d6a96e199824'
            '4f12b61326b8c01e824011af8169280a103d0e6ca451a7fb97d64ce2ebca2477'
            '5317a80a9a1660affa9a83f7cbb9b9a13433c2b6809c5f57c1f5a9b21b887625'
            '105ee161b9d95b8849e0da362a617bbe9ca3d2407f41518922a606eef3256598'
            'a16a4b24a4069baf57c621f0cfc3a1388158e51b0bef73711c2d8eba12d9224e'
            '323db67c76b14ad9809fb5a3118edff9290b27a347494ecf1e35b8fe44b9f30a'
            'a36c62f55fe0e62ac264fa25780436447d4f05cf3f37e7b5aee44b9657958ab1'
            '3ce329b7055036d68c480d9ccb3dd4568229a9aa47d086c8fa8fbc85d24afcc7'
            'dd0c3dd83c2e8b4effb64476d4d55fb7bbcd23463079b162d7f0583ded626ee9'
            'dd45a31922dd3f991510231db2b6bba177b6bba87391359cb3c0f952efe35e8d'
            'b955b1bb9f39dbb682e0c56838222ad3c09079805588bdbf050cd9c95b8117bc'
            '9e3f2ff97b81efe0d30768560304f3e192e189a618211008846ae8d2eff4667a'
            'f5a46311e324a21ba84378f7afcf84a6b63bf30863f87819082abaf69e07ffea'
            '37c03f07e3dfd4797897bc763ab16584329dda38eb669ff0134e5c5bf8c1fee9'
            'bffc4cd687e34432a47c4103fbc3c63dc318baf0e3eaca03bf0265d388dbad3f'
            '74b00cc9a4f3543360d8b356e824269fff6fee2f72ec9aa3e74b9164003cbdc7'
            'e9990ef0b19f76c34538afca009e3133d382d95ba04d73c7fa3c376bf24f775c'
            '2c4f429713ba874018dadcabe7753d6cc04e80cd7f06f8918c1cc866b05a3882'
            '5a1bd1b7a6398f941a2c5a2f106f8e58ba94ff7f0dba8f96fb9e74551e821177'
            '84af604adc392c5e8e4b721219694f0a7c3d7f1c507ec6ffe4cb3349c6bd2d51'
            '2e6b997431443c53449af401bc5d4a3645c55ba3268809cf5c28cdfbe98e379c'
            'd1ccdf91ecfa8feee9c3208cb07c1ba1f5705e6be8a6d864ab565ce200d38c74'
            'e81d6d1fa038672b616689110fc90cf0ab9f0d805201954f942ff1cdc035d13c'
            '7b6d71ac78967c7bcf45c3f9b458874373dcf5ac7b8df451df9dbe7d42e07a6d'
            'd444a5611f6401c44a5b52bcdab4cb4afb117fb826b2639f981b70f4c1d9c423'
            '5b9194b11f214fc7033265ef0f1ed085485318b6fd3028074c8c58115a59a868'
            '44fb4d96ed35ca33a002740aab79e8af56c949053d572f17e6a81badddd6a1b4')

prepare() {
  cd gtk

  # Crash fix
  # https://gitlab.gnome.org/GNOME/gtk/-/merge_requests/8199
  git cherry-pick -n 32381771b1cfa55770036e525a6c53c70be6c920

  local src
  for src in "${source[@]}"; do
    src="${src%%::*}"
    src="${src##*/}"
    [[ $src = *.patch ]] || continue
    msg2 "Applying patch $src..."
    patch -Np1 < "../$src"
  done
}

build() {
  local meson_options=(
    -D broadway_backend=true
    -D cloudproviders=true
    -D colord=yes
    -D gtk_doc=false
    -D demos=false
    -D introspection=true
    -D man=true
    -D tracker=false
    -D tracker3=true
  )

  CFLAGS+=" -DG_DISABLE_CAST_CHECKS"
  arch-meson gtk build "${meson_options[@]}"
  meson compile -C build
}

package() {
  depends+=(gtk-update-icon-cache)
  optdepends=('evince: Default print preview command')
  provides=(
    gtk3=$pkgver
    gtk3-print-backends
    libgailutil-3.so
    libgdk-3.so
    libgtk-3.so
  )
  conflicts=(gtk3 gtk3-print-backends)
  install=gtk3.install

  meson install -C build --no-rebuild --destdir "$pkgdir"

  install -Dm644 /dev/stdin "$pkgdir/usr/share/gtk-3.0/settings.ini" <<END
[Settings]
gtk-icon-theme-name = Adwaita
gtk-theme-name = Adwaita
gtk-font-name = Cantarell 11
END

  install -Dm644 gtk-query-immodules-3.0.hook -t "$pkgdir/usr/share/libalpm/hooks"

  # Built by GTK 4, shared with GTK 3
  rm "$pkgdir/usr/bin/gtk-update-icon-cache"
  rm "$pkgdir/usr/share/man/man1/gtk-update-icon-cache.1"
}

# vim:set ts=2 sw=2 et:
